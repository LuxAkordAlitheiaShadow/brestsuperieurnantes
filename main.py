from scripts.cve import cve
from scripts.apt import apt

def cve_mode():
    print("\nCVE mode")
    apps_to_monitor = [
        'paloaltonetworks',
        'veeam',
        'trendmicro',
        'microsoft',
        'nextcloud',
        'shopify'
    ]
    print("Current enterprise app")
    for app_to_monitor in apps_to_monitor:
        print(app_to_monitor)
    input_value = input("Write a specific app to monitor (another for all)")
    if input_value in apps_to_monitor:
        apps_to_monitor = [input_value]
    cve(apps_to_monitor)

def apt_mode():
    print("\nAPT mode")
    targets_keywords = [
        'France',
        'Manufacturing'
    ]
    for targets_keyword in targets_keywords:
        print(targets_keyword)
    input_value = input("Write a specific target to monitor (another for all)")
    if input_value in targets_keywords:
        targets_keywords = [input_value]
    apt(targets_keywords)


if __name__ == '__main__':
    while True:
        input_value = input("\n1- List CVE\n2- List APT\nPlease choose what do you want to see:\n")
        if input_value == "1":
            cve_mode()
        elif input_value == "2":
            apt_mode()
        else:
            print("See you Nantes!")
            break