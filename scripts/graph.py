import matplotlib.pyplot as plt


def chart_cves_by_severities(cves_severities: dict[str, int]) -> None:
    colors = ['green', 'yellow', 'orange', 'red']
    labels = ['Low', 'Medium', 'High', 'Critical']
    labels_bis = ['0-3','4-7','7-9','9-10']
    numbers = [
        cves_severities['LOW'],
        cves_severities['MEDIUM'],
        cves_severities['HIGH'],
        cves_severities['CRITICAL']
    ]
    title = "CVE by severity"
    pie_chart(labels, numbers, colors, title, labels_bis)


def pie_chart(labels: list[str], values: list[int], colors: list[str], title: str, labels_bis: list[str] | None) -> None:
    explode = (0.1, 0.1, 0.1, 0.1)
    fig1, ax1 = plt.subplots()
    ax1.pie(values, labels=labels, explode=explode, shadow=True, startangle=90, autopct='%1.1f%%', colors=colors)
    ax1.axis('equal')
    plt.title(title)
    if labels_bis:
        plt.legend(labels_bis, title="CVSS score")
    else:
        plt.legend(labels)
    plt.show()