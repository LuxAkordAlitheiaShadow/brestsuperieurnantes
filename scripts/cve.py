import json

from scripts.graph import chart_cves_by_severities


def read_cve_file(cve_year: str) -> dict:
    """
    Dump the JSON content of a CVE list file according to a specific year.

    :param cve_year: CVE year.
    :type cve_year: str
    :return: JSON of CVE as dict.
    :rtype: dict
    """
    with open(f"./CVE/json/nvdcve-1.1-{cve_year}.json", "r") as cve_file:
        cve_file_content = cve_file.read()
        cve_json = json.loads(cve_file_content)
    return cve_json


def concerned_cve(cve: dict, apps_to_monitor: list[str]) -> bool:
    """
    Say if it's a concerned CVE.
    
    :param cve: CVE information
    :type cve: dict
    :param apps_to_monitor: List of applications/technologies to monitor
    :type apps_to_monitor: list[str]
    :return: It's a concerned CVE.
    :rtype: bool
    """
    if cve['configurations']['nodes']:
        cve_node = cve['configurations']['nodes'][0]
        if not cve_node['children']:
            cve_app = cve_node['cpe_match'][0]['cpe23Uri'].split(':')[3]
        else:
            cve_app = cve_node['children'][0]['cpe_match'][0]['cpe23Uri'].split(':')[3]
        if cve_app in apps_to_monitor:
            return True
        else:
            return False


def find_related_cves(apps_to_monitor: list[str]) -> list[dict]:
    """
    Find the related CVEs according to the applaication name in configuration.

    :return: List of related CVEs
    :rtype: list[dict]
    """
    related_cves = []
    for cve_year in range(2022, 2024):
        cve_json = read_cve_file(str(cve_year))
        number_of_cve = int(cve_json['CVE_data_numberOfCVEs'])
        for index_cve in range(0, number_of_cve):
            cve = cve_json['CVE_Items'][index_cve]
            if concerned_cve(cve, apps_to_monitor):
                related_cves.append(cve)
    return related_cves


def print_related_cve(related_cve: dict) -> None:
    """
    Print CVE information

    :param related_cve: Related CVE.
    :type related_cve: dict
    :return: None
    :rtype: None
    """
    print(f"ID: {related_cve['cve']['CVE_data_meta']['ID']} ")
    print(f"CVSS score: {related_cve['impact']['baseMetricV3']['cvssV3']['baseScore']}")
    print(f"CVSS score: {related_cve['impact']['baseMetricV3']['cvssV3']['baseSeverity']}")


def count_severities(cves: list[dict]) -> dict[str, int]:
    """
    Count CVEs severities.

    :param cves: Related CVEs to count
    :type cves: list[dict]
    :return: Number of CVEs severities.
    :rtype: dict[str, int]
    """
    cves_severities = {
        "CRITICAL": 0,
        "HIGH": 0,
        "MEDIUM": 0,
        "LOW": 0,
        "INFO": 0
    }
    for cve in cves:
        cve_severity = cve['impact']['baseMetricV3']['cvssV3']['baseSeverity']
        if cve_severity == "CRITICAL":
            cves_severities["CRITICAL"] = cves_severities["CRITICAL"] + 1
        elif cve_severity == "HIGH":
            cves_severities["HIGH"] = cves_severities["HIGH"] + 1
        elif cve_severity == "MEDIUM":
            cves_severities["MEDIUM"] = cves_severities["MEDIUM"] + 1
        elif cve_severity == "LOW":
            cves_severities["LOW"] = cves_severities["LOW"] + 1
        else:
            cves_severities["INFO"] = cves_severities["INFO"] + 1
    return cves_severities


def print_cves_severities(cves_severities: dict[str, int]) -> None:
    """
    Print CVEs severities.

    :param cves_severities: Count of CVEs severities
    :type cves_severities: dict[str, int]
    :return: None
    :rtype: None
    """
    print("")
    print(f"CRITICAL: {cves_severities['CRITICAL']}")
    print(f"HIGH: {cves_severities['HIGH']}")
    print(f"MEDIUM: {cves_severities['MEDIUM']}")
    print(f"LOW: {cves_severities['LOW']}")
    print(f"INFO: {cves_severities['INFO']}")


def cve(apps_to_monitor: list[str]):
    related_cves = find_related_cves(apps_to_monitor)
    cves_severities = count_severities(related_cves)
    input_value = input("1- CLI\n2- Graph\nPlease select a view mode:\n")
    if input_value == "1":
        for related_cve in related_cves:
            print_related_cve(related_cve)
        print_cves_severities(cves_severities)
    else:
        chart_cves_by_severities(cves_severities)