# Find apt base
import requests
import json


def is_still_active(data):
    if data['properties']['Operations']:
        if 'Date' in data['properties']['Operations'][0]:
            date = data['properties']['Operations'][0]['Date'][-4:]
            return int(date) >= 2016
    else:
        return True


def can_target_us(targets: str, targets_keywords: list[str]) -> bool:
    targets = targets.split('.')
    if len(targets) >= 2:
        targets_one = targets[0].split(':')
        if len(targets_one) == 2:
            targets_one_content = targets_one[1]
        else:
            targets_one_content = targets_one[0]
        for targets_keyword in targets_keywords:
            if targets_keyword in targets_one_content:
                return True
        targets_two = targets[1].split(':')
        if len(targets_two) == 2:
            targets_two_content = targets_two[1]
        else:
            targets_two_content = targets_two[0]
        for targets_keyword in targets_keywords:
            if targets_keyword in targets_two_content:
                return True
    else:
        for targets_keyword in targets_keywords:
            if targets_keyword in targets:
                return True
    return False


def apt(targets_keywords: list[str]):
    url = "https://raw.githubusercontent.com/andreacristaldi/APTmap/master/apt.json"
    response = requests.get(url)
    dataSet = json.loads(response.text)['features']

    # Parse all datas
    for data in dataSet:
        # Check we are targetable by the APT
        targetable = True
        if 'Targets' in data['properties']:
            targetable = can_target_us(data['properties']['Targets'], targets_keywords)
        if targetable:
            active = is_still_active(data)
            if active:
                print(data['properties']['Name'])
