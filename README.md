# BrestSuperieurNantes

Python script to enumerate:
- which APT, active since 2016, can target us according to a list of target sectors and countries
- which CVE (2022-2023) are related to a list of technologies

## Usage

Init. environment

```sh
python3 -m venv .venv
source .venv/bin/activate
```

Install dependencies
```sh
pip install -r requirements.txt
```

Run it
```sh
python3 main.py
```

## Technologie

- AD / Win11
- Palo (paloaltonetworks)
- HP 
- NextCloud (nextcloud:deck)
- TrendMicro (trendmicro:apex_one)
- Veeam (veeam)
- Shopify (shopify)

- (Prestataire) AD / Win10
- (Prestataire) Exchange OnPremise

## Feature

- Print (CLI) CVE information by values
- Print (CLI) CVEs severities by values
- Print (Graph) CVEs severities by values
- Print (CLI) APT by values